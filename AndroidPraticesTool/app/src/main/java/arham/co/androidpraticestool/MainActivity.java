package arham.co.androidpraticestool;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements NetwrukChangeReceiver.ConnectivityReceiverListener{

    Button startService;
    Button stopService;
    View view;
    Context mContext;


    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 100;
     MainActivity mActivity;

    String[] permissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_NETWORK_STATE
            , Manifest.permission.ACCESS_WIFI_STATE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        view = findViewById(R.id.content);
        startService = findViewById(R.id.btnStartService);
        mContext = this;
        mActivity = this;

        startService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent testIntent = new Intent(MainActivity.this,TestActivity.class);
                startActivity(testIntent);
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        permissionCheck();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void permissionCheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkPermissions()) {
                Snackbar.make(view, "Permission Granted", Snackbar.LENGTH_SHORT).show();

            } else {
                //geoDetails();
                Snackbar.make(view, "Permission Denied", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(mContext, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {

            boolean isAllGranted = true;
            if (grantResults.length > 0) {

                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        isAllGranted = false;
                    }
                }

                if (isAllGranted) {

                    Snackbar.make(view, "Permission Granted", Snackbar.LENGTH_SHORT).show();
                } else {

                    if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                        // show an explanation to the user
                        Snackbar.make(view, "Give Permission", Snackbar.LENGTH_SHORT).show();

                        // Good practise: don't block thread after the user sees the explanation, try again to request the permission.
                    } else {
                        showPermissionDialog();
                    }
                }
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                Snackbar.make(view, "All Permission Granted", Snackbar.LENGTH_SHORT).show();
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    private void showPermissionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Need Storage Permission");
        builder.setMessage("This app needs storage permission.");
        builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
       // unregisterReceiver(mNetworkReceiver);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
        MyApplication.getInstance().setConnectivityListener(this);

//
//    }
    }



    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        if (isConnected)
            CommonClass.showSnackBar("Good! Connected to Internet",mActivity,Color.WHITE);
        else
            CommonClass.showSnackBar("Sorry! Not connected to internet",mActivity,Color.RED);


    }
}
