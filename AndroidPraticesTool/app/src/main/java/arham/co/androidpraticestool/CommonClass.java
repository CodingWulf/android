package arham.co.androidpraticestool;

import android.app.Activity;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

class CommonClass {
    static void showSnackBar(String message, Activity mActivity,int color) {



        Snackbar snackbar = Snackbar
                .make(mActivity.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();    }

}
