package arham.co.androidpraticestool;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.support.design.widget.Snackbar;
import android.view.View;

public class MyApplication extends Application {
    public static boolean activityVisible;
    private BroadcastReceiver mNetworkReceiver = new NetwrukChangeReceiver();
     private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        this.registerReceiver(mNetworkReceiver, new IntentFilter(
                "android.net.conn.CONNECTIVITY_CHANGE"));

    }

    public static boolean isActivityVisible()
    {
        return activityVisible;
    }

    public static void activityResumed()
    {
        activityVisible = true;
    }
    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public static void activityPaused()
    {
        activityVisible = false;
    }

    public void setConnectivityListener(NetwrukChangeReceiver.ConnectivityReceiverListener listener) {
        NetwrukChangeReceiver.connectivityReceiverListener= listener;
    }
}
