package arham.co.androidroomandlivedata;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface DaoAccess {
    //Inserting Data
    @Insert
    void insertMultipleRecords(University... university);
    @Insert
    void insertMultipleListRecord(List<University> universities);
    @Insert
    void insertSingleRecord(University university);

    //Fetching Data
    @Query("select * from university")
   LiveData<List<University>> fetchAllData();

    @Query("SELECT * FROM University WHERE clgid =:college_id")
    University getSingleRecord(int college_id);

    //Updating Records
    @Update
    void updateRecord(University university);

    //Deleting Record
    @Delete
    void deleteRecord(University university);

}
