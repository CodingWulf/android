package arham.co.androidroomandlivedata;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.List;

public class MainActivity extends AppCompatActivity implements LifecycleOwner,View.OnClickListener {

    private SampleDatabase database;
    private Button clickMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        clickMe = findViewById(R.id.btnClickMe);
        database = Room.databaseBuilder(getApplicationContext(),
                SampleDatabase.class, "sample-db").build();
        clickMe.setOnClickListener(this);


        LiveData<List<University>> listLiveData = database.doAccess().fetchAllData();
        listLiveData.observe(this, new Observer<List<University>>() {
            @Override
            public void onChanged(@Nullable List<University> universities) {

                assert universities != null;
                int dataSize = universities.size();

                for (int i = 0; i < dataSize - 1; i++)
                {
                    Log.i("Data",universities.get(i).getName()+
                            "--"+universities.get(i).getCollege().getName());
                }

             }
        });
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == clickMe.getId())
        {
            new DatabaseSync().execute();

        }
    }

    private class DatabaseSync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            University university = new University();
            university.setName("Data Wick");

            College college = new College();
            college.setId(10001);
            college.setName("University Of California");
            university.setCollege(college);

            database.doAccess().insertSingleRecord(university);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.i("Database Entry","data Inserted");


         }
    }
}
