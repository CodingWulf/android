package arham.co.androidroomandlivedata;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

//This class name will be the Table name in Database
@Entity
public class University {
    //These variables will be the column name of the Table
    @PrimaryKey(autoGenerate = true)
    private int slNo;
    private String name;
    //Because name variable is common in this class and college class
    //so we add prefix with all variable of college class
    @Embedded(prefix = "clg")
    private College college;

    public University() {
    }

    public int getSlNo() {
        return slNo;
    }

    public void setSlNo(int slNo) {
        this.slNo = slNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }
}

